[![Build Status](https://travis-ci.org/Pryancito/zeusircd.svg?branch=master)](https://github.com/Pryancito/zeusircd)
[![Total alerts](https://img.shields.io/lgtm/alerts/g/Pryancito/zeusircd.svg?logo=lgtm&logoWidth=18)](https://lgtm.com/projects/g/Pryancito/zeusircd/alerts/)
[![Language grade: C/C++](https://img.shields.io/lgtm/grade/cpp/g/Pryancito/zeusircd.svg?logo=lgtm&logoWidth=18)](https://lgtm.com/projects/g/Pryancito/zeusircd/context:cpp)
[![contributions welcome](https://img.shields.io/badge/contributions-welcome-brightgreen.svg?style=flat)](https://github.com/Pryancito/zeusircd/issues)

### Wellcome to the Zeus IRC Daemon [Visit us!](http://www.zeusircd.net)

~~~
 ZeusIRCD is an IRC Daemon writed in C/C++ Lang with
 support to use SQLite3 API Engine and Multi-Lingual
 and an advanced asyncronous engine. Try it !!!
~~~

__HOW TO INSTALL__ [Click Here](https://github.com/Pryancito/zeusircd/wiki)

### Acknowledgment

- [blacknode](https://github.com/blacknode/)
- [Pryancito](https://github.com/Pryancito/)
- [Zoltan](https://github.com/zoltyvigo/)
- Damon
